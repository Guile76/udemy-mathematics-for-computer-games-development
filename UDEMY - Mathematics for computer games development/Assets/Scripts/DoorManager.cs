﻿using UnityEngine;

public class DoorManager : MonoBehaviour
{
    private int doorType = AttributeManager.MAGIC;
    private BoxCollider _collider;

    private void OnCollisionEnter(Collision other)
    {
        if ((other.gameObject.GetComponent<AttributeManager>().attributes & doorType) != 0)
        {
            _collider.isTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _collider.isTrigger = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<BoxCollider>();
    }
}
