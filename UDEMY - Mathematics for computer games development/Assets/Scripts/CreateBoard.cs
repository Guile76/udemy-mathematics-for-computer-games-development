﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CreateBoard : MonoBehaviour
{
    public GameObject[] tilePrefabs;
    public GameObject[] housePrefab;
    public GameObject treePrefab;
    public Text score;
    private GameObject[] _tiles;
    private long _dirtBb;
    private long _desertBb;
    private long _treeBb;
    private long _playerBb;
    
    // Start is called before the first frame update
    void Start()
    {
        _tiles = new GameObject[64];
        for (var r = 0; r < 8; r++)
        {
            for (var c = 0; c < 8; c++)
            {
                var randomTile = UnityEngine.Random.Range(0, tilePrefabs.Length);
                var pos = new Vector3(c, 0, r);
                var tile = Instantiate(tilePrefabs[randomTile], pos, Quaternion.identity);
                tile.name = tile.tag + "_" + r + "_" + c;
                _tiles[r * 8 + c] = tile;
                switch (tile.tag)
                {
                    case "Dirt":
                        _dirtBb = SetCellState(_dirtBb, r, c);
                        break;
                    case "Desert":
                        _desertBb = SetCellState(_desertBb, r, c);
                        break;
                }
            }
        }
        InvokeRepeating("PlantTree", 1, 1);
    }

    void PlantTree()
    {
        var rr = UnityEngine.Random.Range(0, 8);
        var rc = UnityEngine.Random.Range(0, 8);
        if (GetCellState(_dirtBb & ~_playerBb, rr, rc))
        {
            var tree = Instantiate(treePrefab, _tiles[rr * 8 + rc].transform, true);
            tree.transform.localPosition = Vector3.zero;
            _treeBb = SetCellState(_treeBb, rr, rc);
        }
    }

    private int CellCount(long bitboard)
    {
        var count = 0;
        var bb = bitboard;
        while (bb != 0)
        {
            bb &= bb - 1;
            count++;
        }

        return count;
    }

    private void CalculateScore()
    {
        var totalScore = CellCount(_playerBb & _dirtBb) * 10 + CellCount(_playerBb & _desertBb) * 2;
        score.text = "Score: " + totalScore;
    }

    private void PrintBB(string name, long bb)
    {
        Debug.Log(name + ": " + Convert.ToString(bb, 2).PadLeft(64, '0'));
    }

    private long SetCellState(long bitboard, int row, int col)
    {
        var newBit = 1L << (row * 8 + col);
        return (bitboard |= newBit);
    }

    private bool GetCellState(long bitboard, int row, int col)
    {
        var mask = 1L << (row * 8 + col);
        return ((bitboard & mask) != 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetMouseButtonDown(0))
            return;
        
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit))
        {
            var position = hit.collider.transform.position;
            var row = (int) position.z;
            var col = (int) position.x;

            if (GetCellState((_dirtBb | _desertBb) & ~_treeBb, row, col))
            {
                var house = Instantiate(housePrefab[0], hit.collider.transform, true);
                house.transform.localPosition = Vector3.zero;
                _playerBb = SetCellState(_playerBb, row, col);
                CalculateScore();
            }
        }
    }
}
